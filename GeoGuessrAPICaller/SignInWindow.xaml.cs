﻿using GeoGuessrAPICaller.APIConnectionHandlers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace GeoGuessrAPICaller
{
    /// <summary>
    /// Interaction logic for SignInWindow.xaml
    /// </summary>
    public partial class SignInWindow : Window
    {
        public SignInWindow()
        {
            InitializeComponent();
        }

        private void SignInButton_Click(object sender, RoutedEventArgs e)
        {
            SignIn();
        }

        private async void SignIn()
        {
            if (await GeoGuessrAPIService.SignIn(EmailBox.Text, PasswordBox.Password))
            {
                Close();
            }
            else
            {
                MessageBox.Show("Invalid sign in credentials");
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                SignIn();
            }
        }
    }
}
