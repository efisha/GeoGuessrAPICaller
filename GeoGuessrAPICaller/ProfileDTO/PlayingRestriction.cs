namespace GeoGuessrAPICaller.ProfileDTO
{
    public class PlayingRestriction
    {
        public int Restriction { get; set; }
        public bool CanPlayGame { get; set; }
        public string Description { get; set; }
        public Ticket Ticket { get; set; }
        public PeriodicAllowanceMetadata PeriodicAllowanceMetadata { get; set; }
    }
}