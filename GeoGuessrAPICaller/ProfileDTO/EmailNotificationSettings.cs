using System;

namespace GeoGuessrAPICaller.ProfileDTO
{
    public class EmailNotificationSettings
    {
        public bool SendLeagueNotifications { get; set; }
        public bool SendDailyChallengeNotifications { get; set; }
        public bool SendGeneralNotifications { get; set; }
        public DateTime LastCampaignSent { get; set; }
        public string UnsubscribeToken { get; set; }
    }
}