using System;

namespace GeoGuessrAPICaller.ProfileDTO
{
    public class User
    {
        public object Email { get; set; }
        public string Nick { get; set; }
        public DateTime Created { get; set; }
        public bool IsProUser { get; set; }
        public bool ConsumedTrial { get; set; }
        public bool IsVerified { get; set; }
        public Pin Pin { get; set; }
        public int Color { get; set; }
        public string Url { get; set; }
        public string Id { get; set; }
        public string CountryCode { get; set; }
        public Onboarding Onboarding { get; set; }
        public Br Br { get; set; }
        public StreakProgress StreakProgress { get; set; }
        public ExplorerProgress ExplorerProgress { get; set; }
        public int DailyChallengeProgress { get; set; }
        public Progress Progress { get; set; }
        public Competitive Competitive { get; set; }
        public DateTime LastNameChange { get; set; }
        public bool IsBanned { get; set; }
        public DateTime? NameChangeAvailableAt { get; set; }
    }
}