namespace GeoGuessrAPICaller.ProfileDTO
{
    public class Streaks
    {
        public int BrCountries { get; set; }
        public int BrDistance { get; set; }
        public int CsCities { get; set; }
        public int Duels { get; set; }
    }
}