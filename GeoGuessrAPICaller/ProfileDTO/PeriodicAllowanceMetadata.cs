namespace GeoGuessrAPICaller.ProfileDTO
{
    public class PeriodicAllowanceMetadata
    {
        public int GamesLeft { get; set; }
        public int Interval { get; set; }
        public object NextGameUnlocksAt { get; set; }
        public int InfinityGamesLeft { get; set; }
        public object InfinityGameUnlocksAt { get; set; }
    }
}