namespace GeoGuessrAPICaller.ProfileDTO
{
    public class DuelsRank
    {
        public int Rating { get; set; }
        public object Rank { get; set; }
        public int GamesLeftBeforeRanked { get; set; }
        public Division Division { get; set; }
    }
}