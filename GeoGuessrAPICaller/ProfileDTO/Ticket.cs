namespace GeoGuessrAPICaller.ProfileDTO
{
    public class Ticket
    {
        public bool IsTicketActive { get; set; }
        public object ActiveTicketEndsAt { get; set; }
        public int TicketsLeft { get; set; }
        public int DurationPerTicket { get; set; }
        public int TicketWaitTime { get; set; }
        public object NextTicketUnlocksAt { get; set; }
    }
}