namespace GeoGuessrAPICaller.ProfileDTO
{
    public class Progress
    {
        public int Xp { get; set; }
        public int Level { get; set; }
        public int LevelXp { get; set; }
        public int NextLevel { get; set; }
        public int NextLevelXp { get; set; }
        public Title Title { get; set; }
        public BrRank BrRank { get; set; }
        public CsRank CsRank { get; set; }
        public DuelsRank DuelsRank { get; set; }
        public CompetitionMedals CompetitionMedals { get; set; }
        public Streaks Streaks { get; set; }
    }
}