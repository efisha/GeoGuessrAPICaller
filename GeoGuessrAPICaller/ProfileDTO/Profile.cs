namespace GeoGuessrAPICaller.ProfileDTO
{
    public class Profile
    {
        public User User { get; set; }
        public PlayingRestriction PlayingRestriction { get; set; }
        public string Email { get; set; }
        public bool IsEmailChangeable { get; set; }
        public bool IsEmailVerified { get; set; }
        public EmailNotificationSettings EmailNotificationSettings { get; set; }
        public bool IsBanned { get; set; }
        public int DistanceUnit { get; set; }
        public bool HideCustomAvatars { get; set; }
    }
}