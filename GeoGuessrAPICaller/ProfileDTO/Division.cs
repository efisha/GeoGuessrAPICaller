namespace GeoGuessrAPICaller.ProfileDTO
{
    public class Division
    {
        public int Id { get; set; }
        public int DivisionId { get; set; }
        public int TierId { get; set; }
        public int Type { get; set; }
        public int StartRating { get; set; }
        public int EndRating { get; set; }
    }
}