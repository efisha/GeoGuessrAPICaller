namespace GeoGuessrAPICaller.ProfileDTO
{
    public class Br
    {
        public int Level { get; set; }
        public int Division { get; set; }
        public int Streak { get; set; }
    }
}