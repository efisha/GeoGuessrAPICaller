namespace GeoGuessrAPICaller.ProfileDTO
{
    public class Competitive
    {
        public int Elo { get; set; }
        public int Rating { get; set; }
        public int LastRatingChange { get; set; }
        public Division Division { get; set; }
    }
}