namespace GeoGuessrAPICaller.ProfileDTO
{
    public class ExplorerProgress
    {
        public int Bronze { get; set; }
        public int Silver { get; set; }
        public int Gold { get; set; }
        public int Platinum { get; set; }
    }
}