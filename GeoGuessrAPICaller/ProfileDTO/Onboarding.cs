namespace GeoGuessrAPICaller.ProfileDTO
{
    public class Onboarding
    {
        public object TutorialToken { get; set; }
        public string TutorialState { get; set; }
    }
}