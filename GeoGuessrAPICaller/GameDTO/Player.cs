using System.Collections.Generic;

namespace GeoGuessrAPICaller.GameDTO
{
    public class Player
    {
        public TotalScore TotalScore { get; set; }
        public TotalDistance TotalDistance { get; set; }
        public double TotalDistanceInMeters { get; set; }
        public int TotalTime { get; set; }
        public int TotalStreak { get; set; }
        public List<Guess> Guesses { get; set; }
        public bool IsLeader { get; set; }
        public int CurrentPosition { get; set; }
        public Pin Pin { get; set; }
        public List<object> NewBadges { get; set; }
        public List<object> NewObjectives { get; set; }
        public object Explorer { get; set; }
        public string Id { get; set; }
        public string Nick { get; set; }
        public bool IsVerified { get; set; }
    }
}