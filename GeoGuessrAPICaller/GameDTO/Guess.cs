namespace GeoGuessrAPICaller.GameDTO

{
    public class Guess
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
        public bool TimedOut { get; set; }
        public bool TimedOutWithGuess { get; set; }
        public RoundScore RoundScore { get; set; }
        public double RoundScoreInPercentage { get; set; }
        public int RoundScoreInPoints { get; set; }
        public Distance Distance { get; set; }
        public double DistanceInMeters { get; set; }
        public string StreakLocationCode { get; set; }
        public int Time { get; set; }
    }
}