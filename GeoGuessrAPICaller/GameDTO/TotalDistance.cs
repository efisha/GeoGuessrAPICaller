namespace GeoGuessrAPICaller.GameDTO

{
    public class TotalDistance
    {
        public Meters Meters { get; set; }
        public Miles Miles { get; set; }
    }
}