namespace GeoGuessrAPICaller.GameDTO

{
    public class XpProgression
    {
        public int Xp { get; set; }
        public CurrentLevel CurrentLevel { get; set; }
        public NextLevel NextLevel { get; set; }
        public CurrentTitle CurrentTitle { get; set; }
    }
}