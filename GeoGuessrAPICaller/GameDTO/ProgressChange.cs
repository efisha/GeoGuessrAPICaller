using System.Collections.Generic;

namespace GeoGuessrAPICaller.GameDTO
{
    public class ProgressChange
    {
        public List<XpProgression> XpProgressions { get; set; }
        public AwardedXp AwardedXp { get; set; }
        public object PrevRank { get; set; }
        public object NewRank { get; set; }
        public int Medal { get; set; }
        public object SeasonProgress { get; set; }
        public object CompetitiveProgress { get; set; }
    }
}