using System.Collections.Generic;

namespace GeoGuessrAPICaller.GameDTO
{
    public class Game
    {
        public string Token { get; set; }
        public string Type { get; set; }
        public string Mode { get; set; }
        public string State { get; set; }
        public int RoundCount { get; set; }
        public int TimeLimit { get; set; }
        public bool ForbidMoving { get; set; }
        public bool ForbidZooming { get; set; }
        public bool ForbidRotating { get; set; }
        public string StreakType { get; set; }
        public string Map { get; set; }
        public string MapName { get; set; }
        public int PanoramaProvider { get; set; }
        public Bounds Bounds { get; set; }
        public int Round { get; set; }
        public List<Round> Rounds { get; set; }
        public Player Player { get; set; }
        public ProgressChange ProgressChange { get; set; }
    }
}