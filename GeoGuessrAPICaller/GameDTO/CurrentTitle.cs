namespace GeoGuessrAPICaller.GameDTO

{
    public class CurrentTitle
    {
        public int Id { get; set; }
        public int TierId { get; set; }
        public int MinimumLevel { get; set; }
        public string Name { get; set; }
    }
}