namespace GeoGuessrAPICaller.GameDTO

{
    public class Max
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
    }
}