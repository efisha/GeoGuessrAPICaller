namespace GeoGuessrAPICaller.GameDTO

{
    public class NextLevel
    {
        public int Level { get; set; }
        public int XpStart { get; set; }
    }
}