using System.Runtime.Serialization;

namespace GeoGuessrAPICaller.GameDTO

{
    public class Round
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
        public string PanoId { get; set; }
        public double Heading { get; set; }
        public double Pitch { get; set; }
        public double Zoom { get; set; }
        public string StreakLocationCode { get; set; }
        //separated because it's not a part of JSON
        public string StreakLocationName { get; set; }

        [OnDeserialized]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            if (StreakLocationCode is not null)
            {
                var hasName = CountryCodesDictionary.Dictionary.TryGetValue(StreakLocationCode.ToUpper(), out string fullName);
                if (hasName) StreakLocationName = fullName;
            }
        }
    }
}