namespace GeoGuessrAPICaller.GameDTO

{
    public class XpAward
    {
        public int Xp { get; set; }
        public string Reason { get; set; }
        public int Count { get; set; }
    }
}