using System.Collections.Generic;

namespace GeoGuessrAPICaller.GameDTO
{
    public class AwardedXp
    {
        public int TotalAwardedXp { get; set; }
        public List<XpAward> XpAwards { get; set; }
    }

}