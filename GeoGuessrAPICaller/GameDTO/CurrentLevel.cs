namespace GeoGuessrAPICaller.GameDTO

{
    public class CurrentLevel
    {
        public int Level { get; set; }
        public int XpStart { get; set; }
    }
}