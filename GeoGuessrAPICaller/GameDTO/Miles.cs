namespace GeoGuessrAPICaller.GameDTO

{
    public class Miles
    {
        public string Amount { get; set; }
        public string Unit { get; set; }
    }
}