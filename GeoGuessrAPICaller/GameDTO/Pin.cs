namespace GeoGuessrAPICaller.GameDTO

{
    public class Pin
    {
        public string Url { get; set; }
        public string Anchor { get; set; }
        public bool IsDefault { get; set; }
    }
}