namespace GeoGuessrAPICaller.GameDTO

{
    public class Distance
    {
        public Meters Meters { get; set; }
        public Miles Miles { get; set; }
    }
}