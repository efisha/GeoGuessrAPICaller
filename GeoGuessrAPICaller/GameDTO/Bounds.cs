namespace GeoGuessrAPICaller.GameDTO

{
    public class Bounds
    {
        public Min Min { get; set; }
        public Max Max { get; set; }
    }
}