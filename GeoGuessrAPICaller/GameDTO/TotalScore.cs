namespace GeoGuessrAPICaller.GameDTO

{
    public class TotalScore
    {
        public string Amount { get; set; }
        public string Unit { get; set; }
        public double Percentage { get; set; }
    }
}