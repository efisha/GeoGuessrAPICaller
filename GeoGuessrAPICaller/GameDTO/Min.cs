namespace GeoGuessrAPICaller.GameDTO

{
    public class Min
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
    }
}