﻿namespace GeoGuessrAPICaller
{
    public class GameToken
    {
        public string token { get; set; }
        public bool? autoadd { get; set; }
        public string autoadddate { get; set; }
        public string mapSlug { get; set; }
    }
}
