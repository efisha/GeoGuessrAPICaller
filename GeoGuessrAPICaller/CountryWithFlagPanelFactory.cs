﻿using GeoGuessrAPICaller.APIConnectionHandlers;
using GeoGuessrAPICaller.GameDTO;
using Svg;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media.Imaging;

namespace GeoGuessrAPICaller
{
    public static class CountryWithFlagPanelFactory
    {
        public static StackPanel CreateCountryWithFlagPanel(CountryStreakStats stats, int countryIndex)
        {
            Round answer = stats.CountriesGrouped[countryIndex].First();
            Image flag = GetFlagImage(answer);
            StackPanel panel = new()
            {
                Orientation = Orientation.Horizontal
            };
            panel.Children.Add(new Label { Content = string.Format("{0,2:00}", countryIndex + 1) + ".", HorizontalAlignment = HorizontalAlignment.Center });
            panel.Children.Add(new Label { Content = flag, HorizontalAlignment = HorizontalAlignment.Center });
            panel.Children.Add(new Label { Content = answer.StreakLocationName + $" ({stats.CountriesGrouped[countryIndex].Count()})", HorizontalAlignment = HorizontalAlignment.Center });
            return panel;
        }

        private static Image GetFlagImage(Round answer)
        {
            string countryISOCode = answer.StreakLocationCode.ToUpper(CultureInfo.CurrentCulture);
            string flagUrl = "https://www.geoguessr.com/static/flags/" + countryISOCode + ".svg";
            Stream response = GeoGuessrAPIService.client.GetStreamAsync(flagUrl).Result;
            var svgDocument = SvgDocument.Open<SvgDocument>(response);
            
            Image flag = new();
            
            try
            {
                BitmapSource bitmap = Bitmap2BitmapImage(svgDocument.Draw(21, 15));
                flag.Source = bitmap;
                flag.Width = bitmap.Width;
                flag.Height = bitmap.Height;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            
            return flag;
        }

        private static BitmapSource Bitmap2BitmapImage(System.Drawing.Bitmap bitmap)
        {
            return Imaging.CreateBitmapSourceFromHBitmap(bitmap.GetHbitmap(),
                                                         IntPtr.Zero,
                                                         Int32Rect.Empty,
                                                         BitmapSizeOptions.FromWidthAndHeight(bitmap.Width, bitmap.Height));
        }
    }
}
