﻿using GeoGuessrAPICaller.GameDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeoGuessrAPICaller
{
    public class CountryStreakStats
    {
        public const string CountryStreakMapName = "country-streak";

        public List<Game> AllCountryStreakGames { get; set; }
        public List<Game> DefaultCountryStreakGames { get; set; }
        public List<Game> NMCountryStreakGames { get; set; }
        public List<Game> NMPZCountryStreakGames { get; set; }
        public double NumberAllOfGames { get; }
        public double NumberOfDefaultGames { get; }
        public double NumberOfNMGames { get; }
        public double NumberOfNMPZGames { get; }
        public double TotalScoreAll { get; }
        public double TotalScoreDefault { get; }
        public double TotalScoreNM { get; }
        public double TotalScoreNMPZ { get; }
        public List<Round> AllRounds { get; }
        public List<IGrouping<string, Round>> CountriesGrouped { get; }
        public List<AnswersWithCount> AnswersCount { get; }
        public double AverageScoreAll { get; }
        public double AverageScoreDefault { get; }
        public double AverageScoreNM { get; }
        public double AverageScoreNMPZ { get; }
        public double HighScoreAll { get; }
        public double HighScoreDefault { get; }
        public double HighScoreNM { get; }
        public double HighScoreNMPZ { get; }
        public TimeSpan AverageTimePerGuessAll { get; }
        public TimeSpan AverageTimePerGuessDefault { get; }
        public TimeSpan AverageTimePerGuessNM { get; }
        public TimeSpan AverageTimePerGuessNMPZ { get; }
        public double TotalCountriesGuessed { get; }

        public CountryStreakStats(List<Game> allGames)
        {
            AllCountryStreakGames = allGames.Where(x => x.Map == CountryStreakMapName).ToList();
            DefaultCountryStreakGames = AllCountryStreakGames.Where(x => x.ForbidMoving == false && x.ForbidRotating == false && x.ForbidZooming == false).ToList();
            NMCountryStreakGames = AllCountryStreakGames.Where(x => x.ForbidMoving == true && x.ForbidRotating == false && x.ForbidZooming == false).ToList();
            NMPZCountryStreakGames = AllCountryStreakGames.Where(x => x.ForbidMoving == true && x.ForbidRotating == true && x.ForbidZooming == true).ToList();
            NumberAllOfGames = AllCountryStreakGames.Count;
            NumberOfDefaultGames = DefaultCountryStreakGames.Count;
            NumberOfNMGames = NMCountryStreakGames.Count;
            NumberOfNMPZGames = NMPZCountryStreakGames.Count;
            var anyGamesPlayed = NumberAllOfGames != 0;
            var defaultGamesPlayed = NumberOfDefaultGames != 0;
            var nmGamesPlayed = NumberOfNMGames != 0;
            var nmpzGamesPlayed = NumberOfNMPZGames != 0;
            TotalScoreAll = GetTotalScore(anyGamesPlayed, AllCountryStreakGames);
            TotalScoreDefault = GetTotalScore(defaultGamesPlayed, DefaultCountryStreakGames);
            TotalScoreNM = GetTotalScore(nmGamesPlayed, NMCountryStreakGames);
            TotalScoreNMPZ = GetTotalScore(nmpzGamesPlayed, NMPZCountryStreakGames);
            AverageScoreAll = GetAverageScore(anyGamesPlayed, TotalScoreAll, NumberAllOfGames);
            AverageScoreDefault = GetAverageScore(defaultGamesPlayed, TotalScoreDefault, NumberOfDefaultGames);
            AverageScoreNM = GetAverageScore(nmGamesPlayed, TotalScoreNM, NumberOfNMGames);
            AverageScoreNMPZ = GetAverageScore(nmpzGamesPlayed, TotalScoreNMPZ, NumberOfNMPZGames);
            HighScoreAll = GetHighscore(anyGamesPlayed, AllCountryStreakGames);
            HighScoreDefault = GetHighscore(defaultGamesPlayed, DefaultCountryStreakGames);
            HighScoreNM = GetHighscore(nmGamesPlayed, NMCountryStreakGames);
            HighScoreNMPZ = GetHighscore(nmpzGamesPlayed, NMPZCountryStreakGames);
            AverageTimePerGuessAll = GetAverageTime(anyGamesPlayed, AllCountryStreakGames);
            AverageTimePerGuessDefault = GetAverageTime(defaultGamesPlayed, DefaultCountryStreakGames);
            AverageTimePerGuessNM = GetAverageTime(nmGamesPlayed, NMCountryStreakGames);
            AverageTimePerGuessNMPZ = GetAverageTime(nmpzGamesPlayed, NMPZCountryStreakGames);
            AllRounds = AllCountryStreakGames.SelectMany(x => x.Rounds).ToList();
            CountriesGrouped = AllRounds.GroupBy(x => x.StreakLocationName).OrderByDescending(x => x.Count()).ToList();
            TotalCountriesGuessed = CountriesGrouped.Count;
            AnswersCount = CountriesGrouped.Select(x => new AnswersWithCount(x.Key, x.Count())).ToList();
        }

        private static double GetTotalScore(bool gamesPlayed, List<Game> games)
        {
            return gamesPlayed ? games.Select(x => x.Player.TotalStreak).Aggregate((x, y) => x + y) : 0;
        }

        private static double GetAverageScore(bool gamesPlayed, double totalScore, double numberOfGames)
        {
            return gamesPlayed ? totalScore / numberOfGames : 0;
        }

        private static double GetHighscore(bool gamesPlayed, List<Game> games)
        {
            return gamesPlayed ? games.Max(x => x.Player.TotalStreak) : 0;
        }

        private static TimeSpan GetAverageTime(bool gamesPlayed, List<Game> games)
        {
            return gamesPlayed ? TimeSpan.FromSeconds(games.SelectMany(x => x.Player.Guesses).Where(x => x.Time < 86400).Average(x => x.Time)) : TimeSpan.FromSeconds(0);
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            foreach (var country in AnswersCount)
            {
                sb.AppendLine(country.FullName + " ======= " + country.Count);
            }

            sb.AppendLine("Total countries = " + TotalCountriesGuessed);
            sb.AppendLine("Total points = " + TotalScoreAll);
            sb.AppendLine("Total games = " + NumberAllOfGames);
            sb.AppendLine("Average score = " + AverageScoreAll);

            return sb.ToString();
        }
    }
}