﻿using GeoGuessrAPICaller.ActivitiesPageDTO;
using GeoGuessrAPICaller.APIConnectionHandlers;
using GeoGuessrAPICaller.ProfileDTO;
using GeoGuessrAPICaller.UnfinishedGamesDTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace GeoGuessrAPICaller
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void GetStatsButton_Click(object sender, RoutedEventArgs e)
        {
            MainGrid.IsEnabled = false;
            List<GameToken> tokens;

            if (GeoGuessrAPIService.isSignedIn)
            {
                try
                {
                    string activitiesJson = await GeoGuessrAPIService.GetActivities();
                    ActivityPage activityPage = JsonConvert.DeserializeObject<ActivityPage>(activitiesJson);
                    string unfinishedGamesJson = await GeoGuessrAPIService.GetUnfinishedGames();
                    UnfinishedGame unfinishedGames = JsonConvert.DeserializeObject<UnfinishedGame>(unfinishedGamesJson);
                    tokens = TokenProcessor.GetAllTokens(activityPage, unfinishedGames);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    tokens = TokenProcessor.GetExistingTokens();
                }
            }
            else
            {
                tokens = TokenProcessor.GetExistingTokens();
            }

            if (tokens is not null)
            {
                try
                {
                    List<GameDTO.Game> games = GameFactory.DeserializeGames(tokens);
                    CountryStreakStats stats = new(games);
                    StatsViewer.ItemsSource = stats.AnswersCount;
                    AddTop10Flags(stats);
                    AddBottom10Flags(stats);
                    AddStats(stats);
                    ExportStatsButton.IsEnabled = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                ShowInvalidTokens();
            }
            else
            {
                MessageBox.Show("Error reading tokens");
            }
            
            MainGrid.IsEnabled = true;
        }

        private void AddStats(CountryStreakStats stats)
        {
            OverallStatsPanel.Children.Clear();
            DefaultPanel.Children.Clear();
            NMPanel.Children.Clear();
            NMPZPanel.Children.Clear();

            var overallStatsLabels = CreateStatsLabels(stats.NumberAllOfGames, stats.TotalScoreAll, stats.AverageScoreAll, stats.HighScoreAll, stats.AverageTimePerGuessAll);
            foreach (var label in overallStatsLabels)
            {
                OverallStatsPanel.Children.Add(label);
            }

            var defaultPanelLabels = CreateStatsLabels(stats.NumberOfDefaultGames, stats.TotalScoreDefault, stats.AverageScoreDefault, stats.HighScoreDefault, stats.AverageTimePerGuessDefault);
            foreach (var label in defaultPanelLabels)
            {
                DefaultPanel.Children.Add(label);
            }

            var NMPanelLabels = CreateStatsLabels(stats.NumberOfNMGames, stats.TotalScoreNM, stats.AverageScoreNM, stats.HighScoreNM, stats.AverageTimePerGuessNM);
            foreach (var label in NMPanelLabels)
            {
                NMPanel.Children.Add(label);
            }

            var NMPZPanelLabels = CreateStatsLabels(stats.NumberOfNMPZGames, stats.TotalScoreNMPZ, stats.AverageScoreNMPZ, stats.HighScoreNMPZ, stats.AverageTimePerGuessNMPZ);
            foreach (var label in NMPZPanelLabels)
            {
                NMPZPanel.Children.Add(label);
            }
        }

        private List<Label> CreateStatsLabels(double numberOfGames, double totalScore, double averageScore, double highScore, TimeSpan averageTimePerGuess)
        {
            return new List<Label>()
            {
                new Label { Content = "Games played = " + numberOfGames },
                new Label { Content = "Points earned = " + totalScore },
                new Label { Content = "Average score = " + Math.Round(averageScore, 2) },
                new Label { Content = "Highscore = " + highScore },
                new Label { Content = "Avg. time per guess = " + averageTimePerGuess.ToString(@"hh\:mm\:ss") }
            };
        }

        private void AddTop10Flags(CountryStreakStats stats)
        {
            TopPanel.Children.Clear();
            int indexStop = stats.CountriesGrouped.Count >= 10 ? 10 : stats.CountriesGrouped.Count;

            for (int countryIndex = 0; countryIndex < indexStop; countryIndex++)
            {
                StackPanel panel = CountryWithFlagPanelFactory.CreateCountryWithFlagPanel(stats, countryIndex);
                TopPanel.Children.Add(panel);
            }
        }
        
        private void AddBottom10Flags(CountryStreakStats stats)
        {
            BottomPanel.Children.Clear();
            int numberOfCountries = stats.CountriesGrouped.Count;
            int indexStart = numberOfCountries > 10 ? numberOfCountries - 10 : 0;

            for (int countryIndex = indexStart; countryIndex < numberOfCountries; countryIndex++)
            {
                StackPanel panel = CountryWithFlagPanelFactory.CreateCountryWithFlagPanel(stats, countryIndex);
                BottomPanel.Children.Add(panel);
            }
        }

        private async void SignInButton_Click(object sender, RoutedEventArgs e)
        {
            MainGrid.IsEnabled = false;
            if (!GeoGuessrAPIService.isSignedIn)
            {
                SignInWindow signInWindow = new();
                var okPressed = signInWindow.ShowDialog();
                if (GeoGuessrAPIService.isSignedIn)
                {
                    string nick = null;
                    try
                    {
                        string profileJson = await GeoGuessrAPIService.GetProfileInfo();
                        Profile profile = JsonConvert.DeserializeObject<Profile>(profileJson);
                        nick = profile.User.Nick;
                    } catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex.Message);
                        MessageBox.Show("Parsing JSON with your account info went wrong");
                    }

                    SignedInBallLabel.Foreground = Brushes.Green;
                    SignInLabel.Content = nick is null ? "Signed in" : "Signed in as " + nick;
                    SignInButton.Content = "Sign Out";
                }
                else if (okPressed.Value)
                {
                    MessageBox.Show("Error signing in");
                }    
            }
            else
            {
                if (await GeoGuessrAPIService.SignOut())
                {
                    SignedInBallLabel.Foreground = Brushes.Red;
                    SignInButton.Content = "Sign In";
                    SignInLabel.Content = "Signed out";
                }
                else
                {
                    MessageBox.Show("Error signing out");
                }
            }
            MainGrid.IsEnabled = true;
        }

        private static void ShowInvalidTokens()
        {
            if (GameFactory.InvalidTokens.Count > 0)
            {
                StringBuilder sb = new();
                foreach (string invalidToken in GameFactory.InvalidTokens)
                {
                    sb.Append(invalidToken + ", ");
                }
                sb.Remove(sb.Length - 2, 2);

                MessageBox.Show("Invalid tokens: " + sb.ToString() + Environment.NewLine + "(Unfinished games show up as invalid if you are not signed in)");
            }
        }

        private void ExportStatsButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var extension = ".csv";
                var outputFileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "CountryStreakStats");
                if (File.Exists(outputFileName + extension))
                {
                    outputFileName += DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss");
                }
                outputFileName += extension;
                File.WriteAllLines(outputFileName, StatsViewer.ItemsSource.Cast<AnswersWithCount>().Select(x => x.ToString()));
                System.Diagnostics.Process.Start("notepad.exe", outputFileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}

//private static void DownloadFlags(CountryStreakStats stats)
//{
//    string outputDirectory = Directory.CreateDirectory(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "CountryStreakFlags")).FullName;
//    foreach (var country in stats.CountriesGrouped)
//    {
//        string countryCode = country.First().StreakLocationCode.ToUpper();
//        string flagUrl = "https://www.geoguessr.com/static/flags/" + countryCode + ".svg";
//        string outputFile = Path.Combine(outputDirectory, countryCode + ".svg");
//        using (var client = new WebClient())
//        {
//            try
//            {
//                client.DownloadFile(flagUrl, outputFile);
//            }
//            catch (Exception ex)
//            {
//                System.Diagnostics.Debug.WriteLine(ex.Message);
//            }
//        }
//    }
//}