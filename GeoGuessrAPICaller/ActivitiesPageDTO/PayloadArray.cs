﻿using System.Collections.Generic;

namespace GeoGuessrAPICaller.ActivitiesPageDTO
{
    class PayloadArray
    {
        public List<PayloadArrayEntry> PayloadArrayEntries { get; set; }
    }
}
