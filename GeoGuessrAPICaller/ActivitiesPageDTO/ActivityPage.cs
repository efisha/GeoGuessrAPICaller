﻿using System.Collections.Generic;

namespace GeoGuessrAPICaller.ActivitiesPageDTO
{
    public class ActivityPage
    {
        public List<Entry> Entries { get; set; }
        public object PaginationToken { get; set; }
    }
}
