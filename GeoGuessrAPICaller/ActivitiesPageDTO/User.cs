﻿ namespace GeoGuessrAPICaller.ActivitiesPageDTO
{
    public class User
    {
        public string Id { get; set; }
        public string Nick { get; set; }
        public bool IsVerified { get; set; }
        public Avatar Avatar { get; set; }
    }
}
