﻿using System;

namespace GeoGuessrAPICaller.ActivitiesPageDTO
{
    class PayloadArrayEntry
    {
        public int Type { get; set; }
        public DateTime Time { get; set; }
        public Payload Payload { get; set; }
    }
}
