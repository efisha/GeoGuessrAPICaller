﻿namespace GeoGuessrAPICaller.ActivitiesPageDTO
{
    public class Avatar
    {
        public string Url { get; set; }
        public string Anchor { get; set; }
        public bool IsDefault { get; set; }
    }
}
