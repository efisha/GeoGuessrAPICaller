﻿namespace GeoGuessrAPICaller.ActivitiesPageDTO
{
    public class Payload
    {
        public string MapSlug { get; set; }
        public int Points { get; set; }
        public string GameToken { get; set; }
        public string GameMode { get; set; }
    }
}
