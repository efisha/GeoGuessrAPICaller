﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace GeoGuessrAPICaller.ActivitiesPageDTO
{
    public class Entry
    {
        public int Type { get; set; }
        public DateTime Time { get; set; }
        public User User { get; set; }
        public string Payload { get; set; }

        //Added manually
        public List<Payload> Payloads { get; set; }

        [OnDeserialized]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            try
            {
                Payloads = new();
                if (Type == Types.activityTypeSingleGame)
                {
                    Payloads.Add(JsonConvert.DeserializeObject<Payload>(Payload));
                }
                if (Type == Types.activityTypeMultipleGames)
                {
                    DeserializePayloadArray();
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        private void DeserializePayloadArray()
        {
            var sb = new StringBuilder();
            sb.Append("{\"payloadArrayEntries\":");
            sb.Append(Payload);
            sb.Append('}');
            var x = JsonConvert.DeserializeObject<PayloadArray>(sb.ToString());
            foreach (var payloadArrayEntry in x.PayloadArrayEntries)
            {
                if (payloadArrayEntry.Type == Types.gameTypeOffline)
                {
                    Payloads.Add(payloadArrayEntry.Payload);
                }
            }
        }
    }
}
