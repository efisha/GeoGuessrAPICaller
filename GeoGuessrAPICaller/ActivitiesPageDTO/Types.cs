﻿namespace GeoGuessrAPICaller.ActivitiesPageDTO
{
    internal static class Types
    {
        public const int activityTypeSingleGame = 1;
        public const int activityTypeChallenge = 2;
        public const int activityTypeBadgeEarned = 4;
        public const int activityTypeMultipleGames = 7;
        public const int gameTypeOffline = 1;
        public const int gameTypeCompetitive = 6;
    }
}