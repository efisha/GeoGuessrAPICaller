﻿using System;

namespace GeoGuessrAPICaller
{
    public class NotSignedInException : Exception
    {
        public NotSignedInException(string message = "You are not signed in")
            : base(message)
        {
        }

        public NotSignedInException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
