﻿namespace GeoGuessrAPICaller
{
    public class AnswersWithCount
    {
        public int Count { get; set; }
        public string FullName { get; set; }

        public AnswersWithCount(string fullName, int count)
        {
            Count = count;
            FullName = fullName;
        }

        public override string ToString()
        {
            return $"{FullName},{Count}";
        }
    }
}
