﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace GeoGuessrAPICaller.APIConnectionHandlers
{
    public class GeoGuessrAPIService
    {
        private const string signIn = "v3/accounts/signin";
        private const string signOut = "v3/accounts/signout";
        private const string games = "v3/games/";
        private const string stats = "v3/profiles/stats";
        private const string profileInfo = "v3/profiles";
        private const string privateActivities = "v4/feed/private";
        private const string unfinishedGames = "v3/social/events/unfinishedgames";

        public static readonly HttpClient client;
        public static bool isSignedIn;

        static GeoGuessrAPIService()
        {
            client = new HttpClient
            {
                BaseAddress = new Uri("https://www.geoguessr.com/api/"),
            };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public static async Task<bool> SignIn(string email, string password)
        {
            string jsonCredentials = JsonConvert.SerializeObject(new {email, password});

            var request = new HttpRequestMessage(HttpMethod.Post, signIn)
            {
                Content = new StringContent(jsonCredentials, Encoding.UTF8, "application/json")
            };

            HttpResponseMessage outcome = await client.SendAsync(request);

            if (outcome.IsSuccessStatusCode) isSignedIn = true;

            return outcome.IsSuccessStatusCode;
        }

        public static async Task<string> GetStats()
        {
            CheckSignIn();
            return await client.GetStringAsync(stats);
        }

        public static async Task<string> GetProfileInfo()
        {
            CheckSignIn();
            return await client.GetStringAsync(profileInfo);
        }

        public static async Task<string> GetActivities()
        {
            CheckSignIn();
            return await client.GetStringAsync(privateActivities);
        }

        public static async Task<string> GetUnfinishedGames()
        {
            CheckSignIn();
            return await client.GetStringAsync(unfinishedGames);
        }

        private static void CheckSignIn()
        {
            if (!isSignedIn) throw new NotSignedInException();
        }

        public static Task<string> GetGame(string token)
        {
            return client.GetStringAsync(games + token);
        }

        public static async Task<bool> SignOut()
        {
            HttpRequestMessage signOutRequest = new(HttpMethod.Post, signOut)
            {
                Content = new StringContent("{}", Encoding.UTF8, "application/json")
            };
            HttpResponseMessage outcome = await client.SendAsync(signOutRequest);

            if (outcome.IsSuccessStatusCode) isSignedIn = false;

            return outcome.IsSuccessStatusCode;
        }
    }
}