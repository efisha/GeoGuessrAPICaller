﻿using System;
using System.Collections.Generic;
using System.IO;

namespace GeoGuessrAPICaller
{
    public static class CountryCodesDictionary
    {
        public static Dictionary<string, string> Dictionary { get; }

        static CountryCodesDictionary()
        {
            Dictionary = new();
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "CountryCodes.csv")))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    string[] values = line.Split(',');

                    Dictionary.Add(values[1], values[0]);
                }
            }
        }
    }
}
