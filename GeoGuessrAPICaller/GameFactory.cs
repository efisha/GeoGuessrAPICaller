﻿using GeoGuessrAPICaller.APIConnectionHandlers;
using GeoGuessrAPICaller.GameDTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GeoGuessrAPICaller
{
    public static class GameFactory
    {
        public static readonly List<string> InvalidTokens = new();

        public static List<Game> DeserializeGames(List<GameToken> tokens)
        {
            InvalidTokens.Clear();
            List<Game> games = new();

            foreach (GameToken gameToken in tokens)
            {
                try
                {
                    Task<string> json = GeoGuessrAPIService.GetGame(gameToken.token);
                    Game game = JsonConvert.DeserializeObject<Game>(json.Result);
                    games.Add(game);
                }
                catch (Exception)
                {
                    InvalidTokens.Add(gameToken.token);
                }
            }
            return games;
        }
    }
}
