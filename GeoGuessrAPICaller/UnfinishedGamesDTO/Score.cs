﻿namespace GeoGuessrAPICaller.UnfinishedGamesDTO
{
    public class Score
    {
        public string Amount { get; set; }
        public string Unit { get; set; }
        public double Percentage { get; set; }
    }
}
