﻿namespace GeoGuessrAPICaller.UnfinishedGamesDTO
{
    public class Meters
    {
        public string Amount { get; set; }
        public string Unit { get; set; }
    }
}
