﻿using System;
using System.Collections.Generic;

namespace GeoGuessrAPICaller.UnfinishedGamesDTO
{
    public class Game
    {
        public string Token { get; set; }
        public string Map { get; set; }
        public string MapSlug { get; set; }
        public Score Score { get; set; }
        public DateTime DateTime { get; set; }
        public List<Guess> Guesses { get; set; }
        public int Rounds { get; set; }
        public int Round { get; set; }
        public int Type { get; set; }
        public int Mode { get; set; }
    }
}
