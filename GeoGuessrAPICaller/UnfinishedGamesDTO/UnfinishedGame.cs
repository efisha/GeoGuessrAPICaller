﻿using System.Collections.Generic;

namespace GeoGuessrAPICaller.UnfinishedGamesDTO
{
    public class UnfinishedGame
    {
        public List<Game> Games { get; set; }
        public object NextOffset { get; set; }
    }
}
