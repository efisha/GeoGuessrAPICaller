﻿namespace GeoGuessrAPICaller.UnfinishedGamesDTO
{
    public class Guess
    {
        public Score Score { get; set; }
        public Distance Distance { get; set; }
    }
}
