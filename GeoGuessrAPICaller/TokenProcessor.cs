﻿using GeoGuessrAPICaller.ActivitiesPageDTO;
using GeoGuessrAPICaller.UnfinishedGamesDTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace GeoGuessrAPICaller
{
    public class TokenProcessor
    {
        private static readonly string GameTokensFilePath = Path.Combine(Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName, "GameTokens.json");

        public static List<GameToken> GetExistingTokens()
        {
            using (StreamReader reader = new StreamReader(GameTokensFilePath))
            {
                string json = reader.ReadToEnd();
                List<GameToken> gameTokens = JsonConvert.DeserializeObject<List<GameToken>>(json);

                return gameTokens;
            }
        }

        public static List<GameToken> GetAllTokens(ActivityPage activityPage, UnfinishedGame unfinishedGames)
        {
            if (activityPage is null || unfinishedGames is null)
            {
                throw new ArgumentNullException();
            }

            List<GameToken> gameTokens = GetExistingTokens();
            int originalNumberOfTokens = gameTokens.Count;
            List<string> tokens = gameTokens.Select(x => x.token).ToList();

            foreach (Entry entry in activityPage.Entries)
            {
                if (entry.Type == Types.activityTypeSingleGame || entry.Type == Types.activityTypeMultipleGames)
                {
                    foreach (var payload in entry.Payloads)
                    {
                        string token = payload.GameToken;
                        if (!tokens.Contains(token))
                        {
                            AddToken(gameTokens, token, payload.MapSlug);
                        }
                    }
                }
            }

            foreach (Game game in unfinishedGames.Games)
            {
                string token = game.Token;
                if (!tokens.Contains(token))
                {
                    AddToken(gameTokens, token, game.MapSlug);
                }
            }

            if (originalNumberOfTokens != gameTokens.Count)
            {
                UpdateTokensFile(gameTokens);
            }

            return gameTokens;
        }

        private static void AddToken(List<GameToken> tokens, string token, string gameMode)
        {
            GameToken gameToken = new()
            {
                token = token,
                autoadd = true,
                autoadddate = $"{DateTime.Now:yyyy-MM-dd}",
                mapSlug = gameMode
            };
            tokens.Add(gameToken);
        }

        private static void UpdateTokensFile(List<GameToken> gameTokens)
        {
            string updatedJson = JsonConvert.SerializeObject(gameTokens, Formatting.Indented);
            File.WriteAllText(GameTokensFilePath, updatedJson);
        }
    }
}
